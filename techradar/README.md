## [Open Techradar](https://radar.thoughtworks.com/?sheetId=https%3A%2F%2Fgl.githack.com%2FNetworkedAssets%2Fcodepandas-conventions%2Fraw%2Fmaster%2Ftechradar%2Fcodepandas-techradar.csv)

## How to edit
**Note!!!**
Please make sure you preserve the order of rings in first quadrant in `codepandas-techradar.csv` file.

The order should be `using, test-drive, maybe, nope` for the `tools` quadrant (it doesn't matter for the other ones)


## How to propose new stuff

Edit the `codepandas-techradar.csv`, adding new line in following format:

`<name>,maybe,(tools|language-and-frameworks|techniques|platforms),TRUE,<why_we_should_try_it>`